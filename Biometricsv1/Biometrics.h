#ifndef BIOMETRICSLIB_H
#define BIOMETRICSLIB_H

 	#include <mega328P.h>  
    #include <stdio.h>  
    #include <stdlib.h>          
    #include <delay.h> 
    #include "display.h"
    #include <String.h>

    #define  Open               0x01
    #define  Close              0x02
    #define  ChangeBaudRate     0x04
    #define  CmosLed            0x12
    #define  GetEnrollCount     0x20
    #define  CheckEnrolled      0x21
    #define  EnrollStart        0x22
    #define  Enroll1            0x23
    #define  Enroll2            0x24
    #define  Enroll3            0x25
    #define  IsPressed          0x26
    #define  DeleteID           0x40
    #define  DeleteAll          0x41
    #define  Identify           0x51
    #define  CaptureFinger      0x60

//NACK PARAMS
    #define  NACK_TIMEOUT                          0x1001
    #define  NACK_INVALID_BUADRTE                  0x1002
    #define  NACK_INVALID_POS                      0x1003        // verifica si esta o no el usuario         
    #define  NACK_IS_NOT_USED                      0x1004
    #define  NACK_IS_ALREADY_USED                  0x1005
    #define  NACK_COMM_ERR                         0x1006
    #define  NACK_DB_IS_FULL                       0x1009
    #define  NACK_DB_IS_EMPTY                      0x100A
    #define  NACK_BAD_FINGER                       0x100C
    #define  NACK_ENROLL_FAILED                    0x100D
    #define  NACK_IS_NOT_SUPPORTED                 0x100E
    #define  NACK_DEV_ERR                          0x100F
    #define  NACK_INVALID_PARAM                    0x1011
    #define  NACK_FINGER_IS_NOT_PRESSED            0x1012

        
    #define  Ack                0x30
    #define  Nack               0x31      
        
    #define  false              0x00
    #define  true               0x01    
    
            
    //BYTES
    unsigned char highbyte = 0;
    unsigned  char lowbyte = 0; 

    unsigned int checksum = 0;
       
    unsigned char highcheck = 0;
    unsigned  char lowcheck = 0;

    unsigned char response = 0;
    unsigned char lbyte = 0;
    unsigned char hbyte = 0;
    unsigned char checklbyte = 0;
    unsigned char checkhbyte = 0;



    //word
    unsigned int parameterin = 0;
    unsigned int checksumReply = 0;

    // boolean
    unsigned  char communicationError = false;
    unsigned  char checksumCorrect = true; 
    unsigned  char ack = true;


    // Other global variables
    const int transmitDelay = 0;  
    char comando[60];
    int current_id = 0;       
       
    
    
      void calcChecksum(unsigned char c, unsigned char h, unsigned char l)
    { //Also uses this function I have shown above
        checksum = 256 + c + h + l; //adds up all the bytes sent
        highcheck = checksum/256; //then turns this checksum which is a word into 2 bytes
        lowcheck =checksum%256;
    }   
    
    void valueToWORD(int v)
    { //turns the word you put into it (the paramter in the code above) to two bytes
        highbyte = v / 256; //the high byte is the first byte in the word
        lowbyte = v%256; //the low byte is the last byte in the word (there are only 2 in a word)
    }
    
    
     void scannerCommand (char com, int param)
    { //This is the function that sends data to the device
      valueToWORD(param);
      calcChecksum(com, highbyte, lowbyte);
      putchar(0x55);
      putchar(0xaa);
      putchar(0x01);
      putchar(0x00);
      putchar(lowbyte);
      putchar(highbyte);
      putchar(0x00);
      putchar(0x00);
      putchar(com);
      putchar(0x00);
      putchar(lowcheck);
      putchar(highcheck);
    }  
    
    
    void setup()
    {
        DDRB=0x0F; 
        DDRC=0x3F;
        DDRD= 0x0C;
         
        PORTD=0xF0;     
        // USART initialization
        // Communication Parameters: 8 Data, 1 Stop, No Parity
        // USART Receiver: On
        // USART Transmitter: On
        // USART0 Mode: Asynchronous
        // USART Baud Rate: 9600 (Double Speed Mode)
        UCSR0A=0x02;
        UCSR0B=0x18;
        UCSR0C=0x06;
        UBRR0H=0x00;
        UBRR0L=0x0C; 
        ConfiguraLCD();  
 
         
    }
        
    void imprimeComando() 
    {
         char comando[20];   
         BorrarLCD();     
         StringLCD("Comando: 0x");
         sprintf(comando, "%02X", response);
         StringLCDVar(comando);

    }
    
    void imprimeCuenta(){
        char cuenta[20];  
            
         StringLCD("Comando: 0x");
         sprintf(cuenta, "%02X", parameterin);
         StringLCDVar(cuenta);
    }    
  
  
    unsigned char USART_receive(void)
    {
     
         while(!(UCSR0A & (1<<RXC0)));
         return UDR0;
     
    }
           
    
    void ErrorResponse()
    {        
        BorrarLCD();
            switch (parameterin)
            {
                case NACK_TIMEOUT               :        
                                      StringLCD("Connection TimeOut");
                break;
                case  NACK_INVALID_BUADRTE      :   
                                    StringLCD("Invalid Baud Rate");
                break;
                case NACK_INVALID_POS           :  
                                     StringLCD("Invalid ID,Please try another finger, how about the middle one??");
                break;
                case NACK_COMM_ERR              :      
                                       StringLCD("Comunication Error");
                break;
                case NACK_DB_IS_FULL            :   
                                      StringLCD("Data Base is full cannot add any more users");
                break;
                case NACK_ENROLL_FAILED         :  
                                        StringLCD("Enrollment failure.");
                break;
                case NACK_IS_NOT_SUPPORTED      :   
                                       StringLCD("The comand is not suported");
                break;
                case NACK_DEV_ERR               :     
                                        StringLCD("Comando: 0x");
                break;                          
                case NACK_INVALID_PARAM         :      
                        StringLCD("WTF R U Doing!:  Invalid parameter");
                break;
                case NACK_IS_NOT_USED         :      
                        StringLCD("The specified ID is not used.");
                break;

                case NACK_IS_ALREADY_USED         :      
                        StringLCD("The specified ID is already used");
                break;

                case NACK_FINGER_IS_NOT_PRESSED         :      
                        StringLCD("Press your finger just a little bit harder... ;)");
                break;

            };
         }   
         
    unsigned char waitForReply()
    { //This is the function that receives data from the device.
        communicationError = false;
          ack = false; 
           PORTC.5 = 0;   
           PORTC.4 = 0;
        delay_ms(transmitDelay);
        if(getchar() == 0x55)
          {
          }
        else
            {
            communicationError = true;
          }
        if(getchar() == 0xAA)
          {
          }
        else
           {
            communicationError = true;
          }

        if(getchar() == 0x01)
          {
          }
        else
           {
            communicationError = true;
          }    
        if(getchar() == 0x00)
          {
          }
        else
           {
            communicationError = true;
          }          
        lbyte = getchar();
        hbyte = getchar();              
        parameterin = 256*hbyte+ lbyte;            
        getchar();
        getchar();        
        response = getchar();      
        if(response == Ack)
          {
            ack = true;
            PORTC.5 = 1;
          } 
        else 
          {
            ack = false; 
            ErrorResponse();
            PORTC.4 = 1;
          }
        getchar();
        checklbyte = getchar();
        checkhbyte = getchar();
        checksumReply = 256*checkhbyte + checklbyte;        
        if(checksumReply == 256 + lbyte + hbyte + response)
          {
            checksumCorrect = true;         
          }
        else
           {
            checksumCorrect = false;
          } 
          
        return ack & communicationError & checksumCorrect;
    } 
    
    unsigned char validateResponse() {
           if (waitForReply() != true) {
                ErrorResponse();  
                return false;           
             } 
           return true;
    }  
    
    unsigned char VerifyID(){
             scannerCommand(IsPressed,0);
              waitForReply();
              if(!validateResponse)
                return false;
             scannerCommand(CaptureFinger,1);
              waitForReply(); 
             if(!validateResponse)
                return false;
             scannerCommand(Identify,0);
              waitForReply();  
              if(!validateResponse)
                return false;
    }   
                                                            
    
     unsigned char EnrollID(){
             scannerCommand(EnrollStart,current_id); 
             
              if(!validateResponse)
                return false; 
             StringLCD("Seleccionado modo de registro de huella.");  
             delay_ms(2000);
             BorrarLCD();
             StringLCD("Presione su dedo en el sensor."); 
             
             //Finger 1
             scannerCommand(CaptureFinger,1); 
             if(!validateResponse)
                return false;                
             scannerCommand(Enroll1,0); 
             if(!validateResponse)
                return false;   
            BorrarLCD();    
            StringLCD("Retire su dedo del sensor.");     
            scannerCommand(IsPressed,0); 
             if(!validateResponse)
                return false;
             
            while(!parameterin) {
                scannerCommand(IsPressed,0); 
                if(!validateResponse)
                    return false;
            }   
            BorrarLCD();
                
            //Finger 2 
              
            StringLCD("Presione su dedo nuevamente en el sensor."); 
             scannerCommand(CaptureFinger,1); 
             if(!validateResponse)
                return false;
            scannerCommand(Enroll2,0); 
             if(!validateResponse)
                return false;  
             BorrarLCD();   
             StringLCD("Retire su dedo del sensor.");
             scannerCommand(IsPressed,0); 
             if(!validateResponse)
                return false;
             
            while(!parameterin) {    
                delay_ms(100);
                scannerCommand(IsPressed,0); 
                if(!validateResponse)
                    return false;
            }   
            
                    
            //Finger 3
            StringLCD("Presione su dedo nuevamente en el sensor.");  
            scannerCommand(CaptureFinger,1); 
             if(!validateResponse)
                return false; 
            scannerCommand(Enroll3,0); 
            if(!validateResponse)
                return false;
            StringLCD("Retire su dedo del sensor.");
            scannerCommand(CaptureFinger,1); 
             if(!validateResponse)
                return false;   
            while(!parameterin) {    
                delay_ms(100);
                scannerCommand(IsPressed,0); 
                if(!validateResponse)
                    return false;
            }        
            current_id++; 
            BorrarLCD();
            StringLCD("Registro completo, a la verga."); 
            delay_ms(2000);
            BorrarLCD(); 
             
    } 
     unsigned char EnrollIDOM(){
            scannerCommand(GetEnrollCount,0);
            waitForReply();   
            current_id=parameterin;
             scannerCommand(EnrollStart,current_id);
              waitForReply(); 
             StringLCD("Iniciando nuevo registro.");  
             delay_ms(2000);
             BorrarLCD();
             StringLCD("Presione su dedo en el sensor."); 
             
            do{
             scannerCommand(IsPressed,0); 
              waitForReply();
            }while(parameterin != 0);  
            
             //Finger 1 
             
             scannerCommand(CaptureFinger,1);
              waitForReply();                
             scannerCommand(Enroll1,0); 
              waitForReply();  
            BorrarLCD();    
            StringLCD("Retire su dedo del sensor."); 
          
            do{
             scannerCommand(IsPressed,0); 
              waitForReply();
            }while(parameterin == 0);  
            BorrarLCD();
                
            //Finger 2 
            
            StringLCD("Presione su dedo nuevamente en el sensor.");   
            do{
             scannerCommand(IsPressed,0); 
              waitForReply();
            }while(parameterin != 0);  
             scannerCommand(CaptureFinger,1);
              waitForReply();  
           
            scannerCommand(Enroll2,0); 
             waitForReply(); 
             
             BorrarLCD();   
             StringLCD("Retire su dedo del sensor."); 
                  do{
             scannerCommand(IsPressed,0); 
              waitForReply();
            }while(parameterin == 0);  
            BorrarLCD(); 
               
            //Finger 3
            StringLCD("Presione su dedo nuevamente en el sensor.");
            do{
             scannerCommand(IsPressed,0); 
              waitForReply();
            }while(parameterin != 0); 
            scannerCommand(CaptureFinger,1);
             waitForReply();
             scannerCommand(Enroll3,0);
             waitForReply();
            current_id++; 
            BorrarLCD();
            StringLCD("Registro completo, a la verga."); 
            delay_ms(2000);
            BorrarLCD(); 
             
    } 
    
  

#endif
